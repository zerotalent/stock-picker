# Mishal Alajmi
# 15/07/2020
# Contact: [Gitlab: Mishal Alajmi]
#          [Github: Mishal-Alajmi]
#          [email: mishal.ajm@gmail.com]
#
# This program will take a list of stock prices where each index represents the date of the stock,
# and the value at that index will represent the value of that stock.
# The stock_picker() function will then return a subset of that list, that represents the best time to buy and
# best time to sell a stock.


# @params (prices_each_day) each index represents the stock price during that day 
# @output: A sub array representing the best time to buy, and then best time to sell
# for lowest margins [buy, sell]
#
# stock_picker() will call on two functions to determine the best buy and sell options,
# named buy() & sell(). Both will return the index of their respective best option
def stock_picker(stock_prices)
  if stock_prices == nil
    raise "Stock prices list empty"
  end
  puts "\nGiven a list of stock prices #{stock_prices}"
  best_buy_option = buy(stock_prices) # Get the index of the best buying option
  highest_sell_option = sell(stock_prices, best_buy_option) # Get the index of the best sell option
  return [best_buy_option, highest_sell_option]
end

# @params (prices_each_day) each index represents the stock price during that day 
# @output: returns the index of the best day to buy the stock
#
# buy() will create a ranking system represented as a hash to denote the best buying option.
# It will determine that by adding the value of the stock on that day, and where that stock is
# Generally, the lower the result of the addition the better that buying option. The ranking system
# is in grades, where each grade represents a range of values. For example: 
# {A => [0..5], B => [5..10], C => [10..15], D => [15..20], F => anything above that}
# NOTE: This is obviously for toy examples and real stock analysis software will need much more complex
# algorthims. 
def buy(stock_prices)
  buy_score = Hash.new
  # Loop through the option's list to initialize the buy_score hash(key = index of array, and initially all values are F)
  stock_prices.each_with_index do |stock, index|
    buy_score[index] = "F"
  end

  # Loop through the stock hash and assign appropriate value to each option
  buy_score.each do |key, value|
    result = key + stock_prices[key]
    case result
    when 0..5
      value[0] = "A"
    when 5..10 
      value[0] = "B"
    when 10..15 
      value[0] = "C"
    when 15..20 
      value[0] = "D"
    else
      value[0] = "F"
    end
  end
  puts "\nBuying Options: #{buy_score} | Best Option: #{buy_score.select {|key, value| value == "A"}}"
  return buy_score.key("A")
end

# @params (prices_each_day, best_buying_option)
# @output: returns the index of the best day to sell the stock
#
# sell() will pick the stock option(index) which yields the largets number, after subtracting each entry in 
# the stock list with the best buy option provided by the buy() function. 
def sell(stock_prices, buy_candidate)
  margins = Array.new
  highest_profit = 0 # will hold the highest number in the array after subtracting buy_candidate from the stock list
  best_sell_index = 0 # will hold the index of the highest number after subtraction
  stock_prices.each_with_index do |stock, index|
      if buy_candidate > index # if the buy option is after this index then add it to the array as 0
        margins[index] = 0
      else
        margins[index] = stock - stock_prices[buy_candidate]
        if highest_profit < margins[index]
          highest_profit = margins[index]
          best_sell_index = index 
        end
      end
  end
  puts "\nMargins: #{margins} | Highest Profit: $#{highest_profit} | Index of highest profit: #{best_sell_index}"
  return best_sell_index
end

puts "\nHere are the recommended stock choices for [buy,sell]:\t#{stock_picker([17,3,6,9,15,8,6,1,10])}"